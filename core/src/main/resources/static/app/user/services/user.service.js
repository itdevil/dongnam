/**
 * 
 */

angular.module('user').factory('UserService', [ '$http', function($http) {

	return {
		user : "dsjkahdausd",
		isLogged : false,
		login : function(username, password, callBack) {

			$http({
				method : 'POST',
				url : 'api/v1/login',
				headers : {
					'Content-Type' : 'application/json'
				},
				data : {
					'username' : username,
					'password' : password
				}
			}).then(function successCallback(response) {
				// this callback will be called asynchronously
				// when the response is available
				result = response.data
				if (result.code == "0") {
					callBack(true, result);
				} else {
					callBack(false, result.message);
				}

			}, function errorCallback(response) {
				// called asynchronously if an error occurs
				// or server returns response with an error status.
				callBack(false, response.data);
			});
		},
		finduser : function(callBack) {
			$http({
				method : 'GET',
				url : 'api/v1/finduser',
				headers : {
					'Content-Type' : 'application/json'
				}
			}).then(function successCallback(response) {
				// this callback will be called asynchronously
				// when the response is available
				result = response.data
				if (result.code == "0") {
					callBack(true, result);
				} else {
					callBack(false, result.message);
				}

			}, function errorCallback(response) {
				// called asynchronously if an error occurs
				// or server returns response with an error status.
				callBack(false, response.data);
			});
		},
		logout : function(callBack) {
			$http({
				method : 'GET',
				url : 'api/v1/logout',
				headers : {
					'Content-Type' : 'application/json'
				}
			}).then(function successCallback(response) {
				// this callback will be called asynchronously
				// when the response is available
				result = response.data
				if (result.code == "0") {
					callBack(true, result);
				} else {
					callBack(false, result.message);
				}

			}, function errorCallback(response) {
				// called asynchronously if an error occurs
				// or server returns response with an error status.
				callBack(false, response.data);
			});
		}
	}

} ]);
