/**
 * http://usejsdoc.org/
 */
angular.module('user').controller('UserController',
		[ '$scope','$window','UserService', function($scope,$window,UserService) {
			
			$scope.username="";
			$scope.password="";
			
			$scope.login = function(){
				$("#wait").css("display", "block");
				UserService.login($scope.username,$scope.password,function (noError,data){
					
					$("#wait").css("display", "none");
					if(noError){
						UserService.isLogged = true;
						UserService.username = data.user;
						$window.location.href= '#!/donhang';
					}else{
						alert(data);
					}
					
					
				});
			}
			
		} ]);