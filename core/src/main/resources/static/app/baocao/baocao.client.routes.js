/**
 * http://usejsdoc.org/
 */
angular.module('baocao').config([ '$routeProvider', function($routeProvider) {

	$routeProvider

	// route for the doanh thu page
	.when('/doanhthu', {
		templateUrl : 'app/baocao/views/doanhthu.html'
	})

	// route for the ton kho page
	.when('/tonkho', {
		templateUrl : 'app/baocao/views/tonkho.html'
	});
	
} ]);