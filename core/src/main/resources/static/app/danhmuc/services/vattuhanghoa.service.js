/**
 * 
 */

angular.module('danhmuc').factory('UserService', [ '$http', function($http) {

	return {
		getproducts : function(type, callBack) {
			
			$http({
				method : 'POST',
				url : 'api/v1/getproducts',
				headers : {
					'Content-Type' : 'application/json'
				},
				data : {
					'type' : type
				}
			}).then(function successCallback(response) {
				// this callback will be called asynchronously
				// when the response is available
				result = response.data
				if (result.code == "0") {
					callBack(true, result);
				} else {
					callBack(false, result.message);
				}

			}, function errorCallback(response) {
				// called asynchronously if an error occurs
				// or server returns response with an error status.
				callBack(false, response.data);
			});
		}
	}

} ]);
