/**
 * http://usejsdoc.org/
 */

var mainApplicationModuleName = 'dongnam';

var mainApplicationModule = angular.module(mainApplicationModuleName, [
		'ngRoute', 'danhmuc', 'nghiepvu', 'baocao', 'user' ]);

mainApplicationModule.config([ '$locationProvider',
		function($locationProvider) {

			$locationProvider.hashPrefix('!');
		} ]);

angular.element(document).ready(function() {
	angular.bootstrap(document, [ mainApplicationModuleName ]);
	setTimeout(function() {
		$('.selectpicker').selectpicker();
	}, 1000);
});

mainApplicationModule.directive('fileModel', ['$parse', function ($parse) {
    return {
       restrict: 'A',
       link: function(scope, element, attrs) {
          var model = $parse(attrs.fileModel);
          var modelSetter = model.assign;
          
          element.bind('change', function(){
             scope.$apply(function(){
                modelSetter(scope, element[0].files[0]);
             });
          });
       }
    };
 }]);

mainApplicationModule.service('fileUpload', ['$http', function ($http) {
	
    this.uploadFileToUrl = function(file, uploadUrl,callBack){
       var fd = new FormData();
       fd.append('file', file);
    
       $http.post(uploadUrl, fd, {
          transformRequest: angular.identity,
          headers: {'Content-Type': undefined}
       })
    
       .success(function(response){
    	    result = response.data
			if (result.code == "0") {
				callBack(true, result);
			} else {
				callBack(false, result.message);
			}
       })
    
       .error(function(){
    	   callBack(false,response.data)
       });
    }
 }]);

// define MenuBarController
angular.module(mainApplicationModuleName).controller('MenuBarController',
		[ '$scope','$window','UserService', function($scope,$window,UserService) {
			
			$scope.username="";
			$scope.isLogged=false;
			
			$scope.$watch(function() {
				
				return UserService.isLogged;
			}, function handleLoggedChange(newValue) {
				
				$scope.isLogged = newValue;
			});
			
			$scope.$watch(function() {
				return UserService.username;
			}, function handleUsernameChange(newValue) {
				$scope.username = newValue;
			});
			
			$scope.logout = function(){
				UserService.logout(function (noError,data){
					if(noError){
						$window.location.href= '#!/login';
						UserService.isLogged = false;
					}
				})
			}
			
		} ]);
