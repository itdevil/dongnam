/**
 * http://usejsdoc.org/
 */
angular.module('nghiepvu').controller('NhapHoaDonController',
		[ '$scope','$window','UserService', function($scope,$window,UserService) {
			
			if(!UserService.isLogged)
				$window.location.href= '#!/login';
			
			$scope.message="Welcom to nhap hoa don Page";
			setTimeout(function() {
				$('.selectpicker').selectpicker();
			}, 1000);
		} ]);