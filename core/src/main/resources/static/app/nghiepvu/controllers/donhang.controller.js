/**
 * http://usejsdoc.org/
 */
angular.module('nghiepvu').controller('DonHangController',
		[ '$scope','$window','UserService', function($scope,$window,UserService) {
			
			UserService.finduser(function(noError,data){
				
				if(noError){
					UserService.isLogged = true;
					UserService.username = data.user
				}else{
					$window.location.href= '#!/login';
					UserService.isLogged = false;
				}
				
			});
			
			$scope.message="Welcom to don hang Page";
			
			setTimeout(function() {
				$('.selectpicker').selectpicker();
			}, 1000);
			
		} ]);