package com.dongnam.app.businnesslogic;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.dongnam.common.database.DongNamDataSource;
import com.dongnam.common.database.daoimpl.ProductDaoImpl;
import com.dongnam.common.database.entitymodel.Product;

public class VatTuHangHoaBO {
	
	private static final Logger logger = Logger.getLogger(VatTuHangHoaBO.class);
	
	public List<Product> getProducts(String type) throws Exception{
		
		Connection conn = null;
		try {

			conn = DongNamDataSource.getInstance().getConnection();

			ProductDaoImpl dao = new ProductDaoImpl(conn);
			List<Product> list = null;
			if("ALL".equals(type)){
				list = dao.getList();
			}else{
				list = dao.getListbyProductType(type);
			}
			
			return list;

		} catch (Exception e) {
			if (conn != null)
				try {
					conn.rollback();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			e.printStackTrace();
			logger.error(e.getMessage(), e);
			throw e;
		} finally {
			conn.close();
		}

	}

}
