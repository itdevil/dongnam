package com.dongnam.app.businnesslogic;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.web.multipart.MultipartFile;

import com.dongnam.app.controller.user.UserController;
import com.dongnam.common.database.DongNamDataSource;
import com.dongnam.common.database.daoimpl.ProductDaoImpl;
import com.dongnam.common.database.entitymodel.Product;

public class FileUploadHandler {

	private static final Logger logger = Logger.getLogger(UserController.class);

	public boolean proccessFileUpload(String type, MultipartFile file,String username) throws Exception {

		if("LIST_PRODUCT".equals(type))
			return proccessProductFile(file,username);
		
		return false;

	}

	private  boolean proccessProductFile(MultipartFile file, String username) throws Exception {

		Connection conn = null;
		int currentRow = 0;
		try {

			conn = DongNamDataSource.getInstance().getConnection();
			conn.setAutoCommit(false);

			ProductDaoImpl dao = new ProductDaoImpl(conn, username);

			Workbook readWorkbook = WorkbookFactory.create(file.getInputStream());
			Sheet sheet = readWorkbook.getSheetAt(0);

			int lengthRow = sheet.getPhysicalNumberOfRows();
			logger.debug("lengthRow : " + lengthRow);
			List<Product> list = new ArrayList<Product>();
		
			for (int i = 1; i < lengthRow; i++) {
				
				currentRow = i;
				logger.debug("Current row:" + i);
				Row row = sheet.getRow(i);
				String shortName = row.getCell(0).getStringCellValue().trim();
				logger.debug(shortName);
				String fullName = row.getCell(1).getStringCellValue().trim();
				logger.debug(fullName);
				String type = row.getCell(2).getStringCellValue().trim();
				logger.debug(type);
				
				String unit = "";
				if(row.getCell(3) != null && "".equals(row.getCell(3))){
					unit = row.getCell(3).getStringCellValue().trim();
				}
				logger.debug(unit);
						
				String comment = "";
				if(row.getCell(4) != null && "".equals(row.getCell(4))){
					comment = row.getCell(4).getStringCellValue().trim();
				}			 
				logger.debug(comment);
				
				Product product = new Product();
				product.setShortname(shortName);
				product.setFullname(fullName);
				product.setLoai(type);
				product.setUnit(unit);
				product.setComment(comment);
				
				if(!list.contains(product)){
					list.add(product);		
				}
			}
			
			logger.debug("Total inserted : " + list.size());

			dao.insertBatch(list);

			conn.commit();

			return true;

		} catch (Exception e) {
			if (conn != null)
				try {
					conn.rollback();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			e.printStackTrace();
			logger.error(e.getMessage(),e);
			throw new Exception("Error in row index :" + currentRow ) ;
		} finally {
			conn.close();
		}

	}

}
