package com.dongnam.app.storage;

import org.springframework.boot.context.properties.ConfigurationProperties;

import com.dongnam.app.utils.ApplicationConfig;

@ConfigurationProperties("storage")
public class StorageProperties {

    /**
     * Folder location for storing files
     */
    private String location = ApplicationConfig.getInstance().getProp().getProperty("upload_folder");

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

}
