package com.dongnam.app.controller;

import javax.servlet.http.HttpServletRequest;

public class BaseController {
	
	protected String getUsername(HttpServletRequest request){
		return (String) request.getAttribute("user");	
	}

}
