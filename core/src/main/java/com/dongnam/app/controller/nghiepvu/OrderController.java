package com.dongnam.app.controller.nghiepvu;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dongnam.app.controller.user.UserController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Controller
@RequestMapping(value = "/api/v1/", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(value = "/purchaseorder")
public class OrderController {
	
	private static final Logger logger = Logger.getLogger(OrderController.class);

	@RequestMapping(value = "/createOrder", method = RequestMethod.POST)
	@ResponseBody
	@ApiOperation(value = "Login", notes = "for log in action")
	public String login(@RequestBody String postData,HttpServletRequest request, HttpServletResponse response) {

		logger.debug("Message from client:" + postData);
		JSONObject requestData = new JSONObject(postData);
		
		return requestData.toString();

	}
	
	

}
