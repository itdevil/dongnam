package com.dongnam.app.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.dongnam.app.businnesslogic.FileUploadHandler;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Controller
@RequestMapping(value = "/api/v1/", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(value = "/file")
public class FileController {
	
	// E03xx : error from user controller
	private static final Logger logger = Logger.getLogger(FileController.class);

	@RequestMapping(value = "/{type}/upload", method = RequestMethod.POST)
	@ResponseBody
	@ApiOperation(value = "upload file", notes = "for log in action")
	public String handleFileUpload(@PathVariable String type, @RequestParam("file") MultipartFile file,HttpServletRequest request) {

		String username = (String) request.getSession().getAttribute("user");
		JSONObject responseData = new JSONObject();
		FileUploadHandler handler = new FileUploadHandler();
		try {
			if(handler.proccessFileUpload(type, file, username)){
				responseData.put("code", "0");
				responseData.put("filename", file.getOriginalFilename());
				responseData.put("type", type);
			}
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			responseData.put("code", "E0301");
			responseData.put("filename", file.getOriginalFilename());
			responseData.put("message", e.getMessage());
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			responseData.put("code", "E0302");
			responseData.put("filename", file.getOriginalFilename());
			responseData.put("message", e.getMessage());
		}
		
		logger.debug(responseData.toString());
		return responseData.toString();
	}
}
