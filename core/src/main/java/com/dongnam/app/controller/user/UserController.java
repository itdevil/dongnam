package com.dongnam.app.controller.user;

import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dongnam.common.database.DongNamDataSource;
import com.dongnam.common.database.dao.UserDao;
import com.dongnam.common.database.daoimpl.UserDaoImpl;
import com.dongnam.common.database.entitymodel.User;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Controller
@RequestMapping(value = "/api/v1/", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(value = "/user")
public class UserController {

	// E01xx : error from user controller
	private static final Logger logger = Logger.getLogger(UserController.class);

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	@ResponseBody
	@ApiOperation(value = "Login", notes = "for log in action")
	public String login(@RequestBody String postData, HttpServletRequest request, HttpServletResponse response) {

		logger.debug("Message from client:" + postData);
		JSONObject requestData = new JSONObject(postData);
		String username = requestData.getString("username");
		String password = requestData.getString("password");

		JSONObject responseData = new JSONObject();
		Connection conn = null;
		try {
			conn = DongNamDataSource.getInstance().getConnection();
			UserDao dao = new UserDaoImpl(conn);

			User u = dao.getUser(username);

			if (u != null) {
				if (u.getUsername().equals(username) && u.getPassword().equals(password)) {
					responseData.put("code", "0");
					responseData.put("user", u.getFullname());
					request.getSession().setAttribute("user", u);

				} else {
					responseData.put("code", "E0101");
					responseData.put("message", "Wrong Password!");
					request.getSession().removeAttribute("user");
				}
			} else {
				responseData.put("code", "E0102");
				responseData.put("message", "Wrong username!");
				request.getSession().removeAttribute("user");

			}

		} catch (SQLException e) {
			responseData.put("code", "E0103");
			responseData.put("message", e.getMessage());
			request.getSession().removeAttribute("user");
		} finally {
			closeConnection(conn);
		}

		return responseData.toString();

	}

	@RequestMapping(value = "/finduser", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(value = "Find User", notes = "check a user logged in or not")
	public String finduser(HttpServletRequest request, HttpServletResponse response) {

		User user = null;
		if (request.getAttribute("user") != null) {
			user = (User) request.getAttribute("user");
			logger.debug("User is logged:" + user.getFullname());
		}

		JSONObject responseData = new JSONObject();

		if (user != null) {

			responseData.put("code", "0");
			responseData.put("user", user.getFullname());
			request.getSession().setAttribute("user", user);

		} else {
			responseData.put("code", "E0102");
			responseData.put("message", "Wrong username!");
			request.getSession().removeAttribute("user");

		}

		return responseData.toString();

	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(value = "Logout", notes = "for log out action")
	@ApiParam(name = "username")
	public String logout(HttpServletRequest request, HttpServletResponse response) {

		request.getSession().removeAttribute("user");
		JSONObject responseData = new JSONObject();
		responseData.put("code", 0);
		responseData.put("message", "You are logged out");

		return responseData.toString();

	}

	@RequestMapping(value = "/test", method = RequestMethod.GET)
	@ResponseBody
	public String test(HttpServletRequest request, HttpServletResponse response) {

		String test = (String) request.getParameter("username");
		String diem = (String) request.getParameter("diem");

		System.out.println(test);
		System.out.println(diem);
		JSONObject responseData = new JSONObject();
		responseData.put("code", 4);
		responseData.put("message", test);

		return responseData.toString();

	}

	private void closeConnection(Connection conn) {
		if (conn != null)
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}

}
