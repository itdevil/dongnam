package com.dongnam.app.controller.danhmuc;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dongnam.app.businnesslogic.VatTuHangHoaBO;
import com.dongnam.app.controller.BaseController;
import com.dongnam.app.controller.user.UserController;
import com.dongnam.common.database.entitymodel.Product;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Controller
@RequestMapping(value = "/api/v1/", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(value = "/product")
public class ProductController extends BaseController {
	
	// E02xx : error from Product controller
	private final Logger logger = Logger.getLogger(UserController.class);

	@RequestMapping(value = "/getproducts", method = RequestMethod.POST)
	@ResponseBody
	@ApiOperation(value = "get products", notes = "for get all product in database")
	public String getProducts(@RequestBody String postData, HttpServletRequest request, HttpServletResponse response) {

		String user = getUsername(request);
		printDebug(postData,user);
		
		JSONObject requestData = new JSONObject(postData);
		String type = requestData.getString("type");

		JSONObject responseData = new JSONObject();
		try {
			VatTuHangHoaBO bo = new VatTuHangHoaBO();
			List<Product> list = bo.getProducts(type);
	
			responseData.put("code", "0");
			responseData.put("products", new JSONArray(list));
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			responseData.put("code", "E0201");
			responseData.put("message", e.getMessage());
		}

		return responseData.toString();

	}

	private void printDebug(String postData,String user){
		logger.debug("Message from client (" + user +") : " + postData);
	}
}
