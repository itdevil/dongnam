package com.dongnam.app;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static com.google.common.base.Predicates.or;
import static springfox.documentation.builders.PathSelectors.regex;
import com.google.common.base.Predicate;

@Configuration
@EnableSwagger2
@ComponentScan("com.sentifi.core.controllers")
@SuppressWarnings("unchecked")
public class SwaggerConfig {
	@Bean
	public Docket wellcareApis() {
		return new Docket(DocumentationType.SWAGGER_2).groupName("common-apis").apiInfo(apiInfo()).select().paths(fullApiPaths()).build();
	}


	private Predicate<String> fullApiPaths() {
		return or(regex("/api/v1/*.*"));
	}


	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("Sentifi Test").description("Microservice provides some apis for getting trading information").termsOfServiceUrl("https://sentifi.com/").contact(new Contact("Sentifi", "https://sentifi.com/", "contact@sentifi.com"))
				// .license("Apache License Version 2.0")
				.license("Sentifi Test Version 1.0")
				// .licenseUrl("https://github.com/springfox/springfox/blob/master/LICENSE")
				.licenseUrl("https://sentifi.com/").version("1.0").build();
	}
}