package com.dongnam.app.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class ValidateUtils {
	
	public static boolean isValiDate(String dateToValidate, String dateFromat){

		if(dateToValidate == null){
			return false;
		}

		SimpleDateFormat sdf = new SimpleDateFormat(dateFromat);
		sdf.setLenient(false);

		try {

			//if not valid, it will throw ParseException
			Date date = sdf.parse(dateToValidate.trim());
			System.out.println(date);
			return true;
		} catch (ParseException e) {
			return false;
		} 
	}
	
	public static boolean validateDateRange(String startDate,String endDate){
		
		if(startDate.compareTo(endDate) <= 0)
			return true;
		
		 return false;
		
	}

}
