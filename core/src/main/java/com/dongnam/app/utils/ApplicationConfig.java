package com.dongnam.app.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ApplicationConfig {

	private Properties prop;
	private String ticker[];

	public Properties getProp() {
		return prop;
	}

	public void setProp(Properties prop) {
		this.prop = prop;
	}

	private static ApplicationConfig appConfig = new ApplicationConfig();

	private ApplicationConfig(){
        InputStream inputStream = null;
        
        try {
        	prop = new Properties();
            String propFileName = "application.properties";
            inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
            if (inputStream != null) {
            	prop.load(inputStream);

                inputStream.close();
            }
        } catch (Exception e) {
        }finally{
        	if(inputStream != null){
        		try {
					inputStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        	}
        }
    }

	public static ApplicationConfig getInstance() {
		return appConfig;
	}

	public String[] getTicker() {
		return ticker;
	}

	public void setTicker(String[] ticker) {
		this.ticker = ticker;
	}
	
}
