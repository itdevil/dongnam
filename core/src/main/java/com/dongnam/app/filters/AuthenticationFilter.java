package com.dongnam.app.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.JSONObject;

@WebFilter
public class AuthenticationFilter implements Filter {

	private static final Logger logger = Logger.getLogger(AuthenticationFilter.class);

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain arg2)
			throws IOException, ServletException {
		// TODO Auto-generated method stub

		HttpServletRequest request = (HttpServletRequest) arg0;
		HttpServletResponse response = (HttpServletResponse) arg1;

		JSONObject responseData = new JSONObject();
		String requestUrl = request.getRequestURL().toString();
		if (requestUrl.contains("/api/v1")) {

			if (requestUrl.contains("/api/v1/login")) {
				arg2.doFilter(arg0, arg1);
			} else {
				if (request.getSession().getAttribute("user") != null) {

					arg0.setAttribute("user", request.getSession().getAttribute("user"));
					arg2.doFilter(arg0, arg1);

				} else {
					responseData.put("code", 0);
					responseData.put("message", "You are logged out");
					response.getWriter().write(responseData.toString());
					response.flushBuffer();
				}
			}

		} else {
			arg2.doFilter(arg0, arg1);
		}

	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub

	}

}
