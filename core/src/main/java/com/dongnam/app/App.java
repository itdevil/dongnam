package com.dongnam.app;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.dongnam.app.filters.AuthenticationFilter;
import com.dongnam.app.storage.StorageProperties;
import com.dongnam.app.storage.StorageService;
import com.dongnam.common.utils.CommonConfigUtil;

@Configuration
@EnableScheduling
@SpringBootApplication
@EnableConfigurationProperties(StorageProperties.class)
public class App extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(App.class);
	}

	public static void main(String[] args) throws Exception {

		SpringApplication.run(App.class, args);
		CommonConfigUtil.getInstance();
	}

	@Bean
	CommandLineRunner init(StorageService storageService) {
		return (args) -> {
			storageService.deleteAll();
			storageService.init();
		};
	}
	
	@Bean
	 public FilterRegistrationBean authenticationFilter() {
	  FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
	  AuthenticationFilter filter = new AuthenticationFilter();
	  filterRegistrationBean.setFilter(filter);
	  filterRegistrationBean.addUrlPatterns("/*");
	  return filterRegistrationBean;
	 }


}
