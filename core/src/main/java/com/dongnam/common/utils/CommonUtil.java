package com.dongnam.common.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
public class CommonUtil {

	static private final Logger logger = Logger.getLogger(CommonUtil.class);
	/**
	 * get file name without path
	 */
	public static String getFileName(String fullPath) {
		File f = new File(fullPath);
		if (f.exists())
			return f.getName();
		return null;

	}

	/**
	 * 
	 * @param fullPath
	 * @return file extension
	 */
	public static String getExtension(String fullPath) {
		int dot = fullPath.lastIndexOf(".");
		return fullPath.substring(dot + 1);
	}

	/**
	 * 
	 * @return current date
	 */
	public static String getCurrentDate() {
		SimpleDateFormat dateFortmat = new SimpleDateFormat("dd-MM-yyyy");
		Date currentDate = new Date();
		String date_in = dateFortmat.format(currentDate);

		return date_in;
	}

	public static String formatDate(Date date, String pattern) {
		SimpleDateFormat dateFortmat = new SimpleDateFormat(pattern);
		String date_in = dateFortmat.format(date);

		return date_in;
	}

	public static Date stringtoDate(String pattern, String dateString) {
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		try {

			Date date = formatter.parse(dateString);

			logger.debug(date);

			return date;

		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;

	}

	/**
	 * 
	 * @return current time
	 */
	public static Timestamp getCurrentTimeStamp() {

		Date today = new Date();
		return new Timestamp(today.getTime());

	}
	
	/**
	 * 
	 * @return current time
	 */
	public static int getCurrentYear() {

		Calendar cal = Calendar.getInstance();
		Date today = new Date();
		cal.setTime(today);
		return cal.get(Calendar.YEAR);

	}

	/**
	 * 
	 * @param list
	 * @return buils a string with syntx ('x','aaa') it is used for select query
	 */
	public static String builStringList(List<String> list) {

		StringBuilder builder = new StringBuilder();
		int length = list.size();
		builder.append("(");
		for (int i = 0; i < length - 1; i++) {
			builder.append("'").append(list.get(i)).append("'").append(",");
		}
		builder.append("'").append(list.get(length - 1)).append("'").append(")");
		return builder.toString();

	}

	/**
	 * 
	 * @param list
	 * @return buils a string with syntx ('x','aaa') it is used for select query
	 */
	public static String builStringList(HashSet<String> list) {

		StringBuilder builder = new StringBuilder();
		builder.append("(");
		for (String item : list) {
			builder.append("'").append(item).append("'").append(",");
		}

		int lastIndexOfComma = builder.lastIndexOf(",");
		builder.replace(lastIndexOfComma, lastIndexOfComma + 1, ")");

		return builder.toString();

	}

	/**
	 * 
	 * @param path
	 *            of the file
	 * @param data
	 *            write string to a file
	 */
	public static void writeStringToFile(String path, String data) {
		try {

			File file = new File(path);

			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}

			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "utf-8"));
			bw.write(data);
			bw.close();

			System.out.println("Done create file language");

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @param s
	 * @return true if s is null and empty
	 */
	public static boolean isEmptyString(String s) {
		if (s != null && !s.isEmpty()) {
			return false;
		}

		return true;
	}


	/**
	 * 
	 * @param pattern
	 * @param value
	 * @return string which is formated base on pattern
	 */
	static public String customFormat(String pattern, double value) {
		DecimalFormat myFormatter = new DecimalFormat(pattern);
		return myFormatter.format(value);
	}

	static public String getDelimiter(String pathFile) {

		BufferedReader in = null;
		try {
			in = new BufferedReader(new InputStreamReader(new FileInputStream(pathFile), "UTF-8"));

			String firstLine = in.readLine();

			if (firstLine != null) {

				if (firstLine.contains(";")) {
					return ";";
				} else if (firstLine.contains(",")) {
					return ",";
				} else {
					return "";
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}

		return null;
	}

	public static String getStringWithDelimiterFromArray(String[] values) {

		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < values.length; i++) {
			builder.append('\'').append(values[i].trim()).append('\'').append(",");
		}

		builder.deleteCharAt(builder.lastIndexOf(","));

		return builder.toString();

	}


	/**
	 * 
	 * @param filename
	 * @return, return a type of a file which will be convert
	 */
	public static String getFileType(String pathName) {

		String extension = getExtension(pathName);

		if ("xls".equalsIgnoreCase(extension) || "xlsx".equalsIgnoreCase(extension)) {

			return CommonContants.FILE_TYPE_EXCEL;

		} else if ("xml".equalsIgnoreCase(extension)) {

			return CommonContants.FILE_TYPE_XML;
		} else {

			String delimiter = CommonUtil.getDelimiter(pathName);
			if (delimiter != null) {
				if (";".equals(delimiter)) {
					logger.debug("TextFiletoCsv with ';' delimiter was executed");
					return CommonContants.FILE_TYPE_TEXT;
				} else if (",".equals(delimiter)) {
					logger.debug("TextFiletoCsv with ',' delimiter was executed");
					return CommonContants.FILE_TYPE_TEXT;
				} else {
					logger.debug("FixedLengthFileToCsv  was executed");
					return CommonContants.FILE_TYPE_FIXED_LENGTH;
				}
			}

		}

		return null;

	}

	public static XMLGregorianCalendar createXMLGregorianDate(Date date) throws DatatypeConfigurationException {
		GregorianCalendar gcal = new GregorianCalendar();
		gcal.setTime(new Date());
		XMLGregorianCalendar xgcal = DatatypeFactory.newInstance().newXMLGregorianCalendar(gcal);

		return xgcal;
	}

	public static boolean writeByteToFile(byte []bytes,String path) throws IOException{
		FileOutputStream stream = new FileOutputStream(path);
		try {
		    stream.write(bytes);
		    return true;
		} finally {
		    stream.close();
		}
	}
	public static byte[] FiletoByteArray(String pathFile) {

		ByteArrayOutputStream bos = null;
		FileInputStream fis = null;
		try {
			File file = new File(pathFile);
			byte[] buf = new byte[1024];
			bos = new ByteArrayOutputStream();
			fis = new FileInputStream(file);
			for (int readNum; (readNum = fis.read(buf)) != -1;) {
				bos.write(buf, 0, readNum);
			}

			byte[] bytes = bos.toByteArray();

			return bytes;
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (bos != null) {
				try {
					bos.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return null;

	}


	public static String objectToJsonString(Object obj) {

		// Object to JSON in String
		String jsonInString = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			jsonInString = mapper.writeValueAsString(obj);
			return jsonInString;
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e.getMessage(),e);
		}
		return jsonInString;
	}

	public static Object jsonStringToObject(String jsonString, @SuppressWarnings("rawtypes") Class className) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			@SuppressWarnings("unchecked")
			Object obj = mapper.readValue(jsonString, className);
			return obj;

		}  catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e.getMessage(),e);
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e.getMessage(),e);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e.getMessage(),e);
		}
		return null;
	}
	
}
