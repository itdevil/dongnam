package com.dongnam.common.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.dongnam.app.controller.user.UserController;

/**
 * class for load log and configure
 * 
 * @author loc.dang
 *
 */
public class CommonConfigUtil {

	private static final Logger logger = Logger.getLogger(CommonConfigUtil.class);

	private static CommonConfigUtil configutil = new CommonConfigUtil();
	private Properties prop;

	private CommonConfigUtil() {

		loadAppConfig();
		// TODO Auto-generated constructor stub

	}

	public static CommonConfigUtil getInstance() {
		return configutil;
	}

	public Properties getProp() {
		return prop;
	}

	public void setProp(Properties prop) {
		this.prop = prop;
	}
	
	/**
	 * load application configure file from resource
	 */
	private void loadAppConfig() {
		String propFileName = "configCommon.properties";
		InputStream inputStream = null;
		try {

			prop = new Properties();

			inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
			logger.info("Configuration info is loaded from :" + propFileName);

			if (inputStream != null) {
				prop.load(inputStream);
			} else {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			}

			logger.info("Wow! Finished load configured application of Common!");
			logger.info("properties of Common:" + prop);

		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try {
				inputStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println(e.getMessage());
			}
		}
	}

}
