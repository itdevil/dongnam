package com.dongnam.common.utils;

//multi-language Kien_test

//end Kien_test

public class CommonContants {



	final public static String NEW_LINE_SEPARATOR = "\n";
	final public static char DELIMITER = ';';

	final public static String FILE_TYPE_EXCEL = "excel";
	final public static String FILE_TYPE_TEXT = "text";
	final public static String FILE_TYPE_FIXED_LENGTH = "fixed_length";
	final public static String FILE_TYPE_XML = "xml";
	
}
