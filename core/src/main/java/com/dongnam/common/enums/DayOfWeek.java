package com.dongnam.common.enums;

public enum DayOfWeek {

	SUNDAY(0),MONDAY(1), TUESDAY(2), WENESDAY(3),THURSDAY(4),FRIDAY(5),SATURDAY(6);

	public final int id;

	private DayOfWeek(int id) {
		this.id = id;
	}

	public static DayOfWeek getLabel(int id) {
		for (DayOfWeek obj : DayOfWeek.values()) {
			if (obj.id == id) {
				return obj;
			}
		}
		return null;
	}

}
