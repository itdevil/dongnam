package com.dongnam.common.database.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.sql.SQLTimeoutException;
import java.util.List;

import org.apache.log4j.Logger;

import com.dongnam.common.database.GenericDao;
import com.dongnam.common.database.dao.UserDao;
import com.dongnam.common.database.entitymodel.User;
import com.dongnam.common.database.sqlutil.SQLUtils;

public class UserDaoImpl extends GenericDao<User> implements UserDao {

	private final Logger logger = Logger.getLogger(UserDaoImpl.class);
	
	public UserDaoImpl(Connection connection) {
		super(connection);
		// TODO Auto-generated constructor stub
	}

	@Override
	public User getUser(String userName) throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException {
		// TODO Auto-generated method stub
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			String query = SQLUtils.getUserQueries().getSqlcommand().get("GetUserByUserName");
			System.out.println();
			statement = connection.prepareStatement(query);
			
			statement.setString(1, userName);
			
			resultSet = statement.executeQuery();
			if(resultSet.next()){
				return createUserData(resultSet);
			}
		
		} finally {
			closeStatment(statement,resultSet);
		}
		return null;
	}

	private User createUserData(ResultSet resultSet) throws SQLException {
		User user =  new User(resultSet.getLong("id"), resultSet.getString("username"), resultSet.getString("fullname"),
				resultSet.getString("password"));
		
		logger.debug(user.toString());
		
		return user;
	}

	private void setParamForInsertStatment(PreparedStatement statement, User data) throws SQLException {
		statement.setString(1, data.getUsername());
		statement.setString(2, data.getPassword());
		statement.setString(3, data.getFullname());
	}

	private void setParamForUpdateStatment(PreparedStatement statement, User data) throws SQLException {
		statement.setString(1, data.getUsername());
		statement.setString(2, data.getPassword());
		statement.setString(3, data.getFullname());

		statement.setLong(1, data.getId());
	}

	@Override
	public int insert(User data) throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int insertBatch(List<User> entities)
			throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int update(User entity) throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int updateBatch(List<User> entities)
			throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int delete(User entity) throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int deleteBatch(List<User> entities)
			throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<User> getList() throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User getRecord(Long id) throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException {
		// TODO Auto-generated method stub
		return null;
	}

}
