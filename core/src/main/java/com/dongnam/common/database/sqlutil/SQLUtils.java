package com.dongnam.common.database.sqlutil;

import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.Logger;

import com.dongnam.common.database.daoimpl.UserDaoImpl;
import com.dongnam.common.database.sqlutil.model.Queries;

public class SQLUtils {

	private static final Logger logger = Logger.getLogger(UserDaoImpl.class);
	
	private static SQLUtils sqlUtils = new SQLUtils();
	
	private Queries userQueries; 
	private Queries productQueries;
	
	private SQLUtils() {
		super();
		userQueries = xmlToObject("SQL_UserImpl.xml");
		productQueries = xmlToObject("SQL_ProductImpl.xml");
		
	}

	private Queries xmlToObject(String xml) {
		try {

			// create JAXB context and initializing Marshaller
			JAXBContext jaxbContext = JAXBContext.newInstance(Queries.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			// specify the location and name of xml file to be read
			InputStream xmlStrema =  SQLUtils.class.getClassLoader().getResourceAsStream(xml);
			// this will create Java object - Queries from the XML file
			Queries queries = (Queries) jaxbUnmarshaller.unmarshal(xmlStrema);
			logger.debug(queries.toString());
			
			return queries;
		} catch (JAXBException e) {
			// some exception occured
			e.printStackTrace();

		}
		return null;

	}
	
	public static Queries getUserQueries(){
		return sqlUtils.userQueries;
	}
	
	public static Queries getProductQueries(){
		return sqlUtils.productQueries;
	}
	
	
}
