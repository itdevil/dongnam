package com.dongnam.common.database.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.sql.SQLTimeoutException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.dongnam.common.database.GenericDao;
import com.dongnam.common.database.dao.ProductDao;
import com.dongnam.common.database.entitymodel.Product;
import com.dongnam.common.database.entitymodel.User;
import com.dongnam.common.database.sqlutil.SQLUtils;

public class ProductDaoImpl extends GenericDao<Product> implements ProductDao {

	private final Logger logger = Logger.getLogger(ProductDaoImpl.class);
	
	
	public ProductDaoImpl(Connection connection) {
		super(connection);
		// TODO Auto-generated constructor stub
	}

	public ProductDaoImpl(Connection connection,String username) {
		super(connection,username);
		// TODO Auto-generated constructor stub
	}

	@Override
	public int insert(Product data) throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	private void setParamForInsert(PreparedStatement statement ,Product product ) throws SQLException{
		
		setBaseInformation(product);
		statement.setString(1, product.getShortname());
		statement.setString(2, product.getFullname());
		statement.setString(3, product.getUnit());
		statement.setString(4, product.getLoai());
		statement.setString(5, product.getComment());
		statement.setString(6, product.getCreate_by());
		statement.setObject(7, product.getCreate_on());
		statement.setString(8, product.getUpdate_by());
		statement.setObject(9, product.getUpdate_on());
	}
	
	private Product getValueFromResulSet(ResultSet resultSet ) throws SQLException{
		
		Long id = resultSet.getLong("id");
		String shortname = resultSet.getString("shortname");
		String fullname = resultSet.getString("fullname");
		String unit = resultSet.getString("donvitinh");
		String ghichu = resultSet.getString("ghichu");
		String loai = resultSet.getString("loai");
		
		Product product = new Product(id, shortname, fullname, unit, ghichu, loai);
		
		setBaseValueFromResultset(product,resultSet);
		
		return product;
		
	}
	
	
	@Override
	public int insertBatch(List<Product> entities)
			throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException {
		// TODO Auto-generated method stub
		
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			String query = SQLUtils.getProductQueries().getSqlcommand().get("InsertBatchProduct");
			logger.debug("query:" + query);
			statement = connection.prepareStatement(query);
			int length = entities.size();
			for (int i = 0;i<length;i++){
				
				setParamForInsert(statement,entities.get(i));
				statement.addBatch();
			}
			
			statement.executeBatch();
			return 1;
		
		} finally {
			closeStatment(statement,resultSet);
		}
		
	}

	@Override
	public int update(Product entity) throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int updateBatch(List<Product> entities)
			throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int delete(Product entity) throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int deleteBatch(List<Product> entities)
			throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<Product> getList() throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException {
		// TODO Auto-generated method stub
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			String query = SQLUtils.getProductQueries().getSqlcommand().get("getAll");
			statement = connection.prepareStatement(query);
			
			resultSet = statement.executeQuery();
			List<Product> list = new ArrayList<>();
			while(resultSet.next()){
				Product product = getValueFromResulSet(resultSet);
				list.add(product);
			}
			
			return list;
		} finally {
			closeStatment(statement,resultSet);
		}
		
	}
	
	public List<Product> getListbyProductType(String type) throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException {
		// TODO Auto-generated method stub
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			String query = SQLUtils.getProductQueries().getSqlcommand().get("getByProductType");
			statement = connection.prepareStatement(query);
			statement.setString(1, type);
			
			resultSet = statement.executeQuery();
			List<Product> list = new ArrayList<>();
			while(resultSet.next()){
				Product product = getValueFromResulSet(resultSet);
				list.add(product);
			}
			
			return list;
		} finally {
			closeStatment(statement,resultSet);
		}
	}
	
	

	@Override
	public Product getRecord(Long id) throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException {
		// TODO Auto-generated method stub
		return null;
	}

}
