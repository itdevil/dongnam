package com.dongnam.common.database;

import java.util.Set;
import java.util.TreeSet;

import org.apache.log4j.Logger;


/**
 * 
 * @author dangtan.loc
 *
 */
public class Criteria {

	private String tableName; // name of table which is made query search
	private TreeSet<String> resultColumns; // list of column will be return of
											// the query
	private Set<Restriction> restrictions; // list of Restriction for the query
	private Set<Restriction> restrictions_OR; // list of Restriction which will
												// be group in o bracket and use
												// "OR" expression for them

	private final Logger logger = Logger.getLogger(Criteria.class);

	public Criteria(String tableName, TreeSet<String> resultColumns, Set<Restriction> restrictions, Set<Restriction> restrictions_OR) {
		super();
		this.tableName = tableName;
		this.resultColumns = resultColumns;
		this.restrictions = restrictions;
		this.restrictions_OR = restrictions_OR;
	}

	public String createCriteriaQuery() {
		StringBuilder query = new StringBuilder();

		query.append("SELECT ");
		if (resultColumns != null && !resultColumns.isEmpty()) {

			for (String column : resultColumns)
				query.append(column).append(", ");

			query.deleteCharAt(query.lastIndexOf(","));
		}
		logger.debug(query.toString());

		query.append("FROM ").append(this.tableName).append(" ");

		if (restrictions != null && !restrictions.isEmpty()) {
			query.append("WHERE ");

			for (Restriction column : restrictions)
				query.append(column.getColumnName()).append(" ").append(column.getRestriction()).append(" ").append("?").append("and ");

			int indexAnd = query.lastIndexOf("and");
			query.delete(indexAnd, indexAnd + 3);

			logger.debug(query.toString());
			if (restrictions_OR != null && !restrictions_OR.isEmpty()) {

				query.append("and (");
				for (Restriction column : restrictions_OR)
					query.append(column.getColumnName()).append(" ").append(column.getRestriction()).append(" ").append("?").append(" or ");

				int indexOr = query.lastIndexOf("or");
				query.delete(indexOr, indexOr + 2);
				query.append(")");
			}
			logger.debug(query.toString());
		} else {
			if (restrictions_OR != null && !restrictions_OR.isEmpty()) {
				query.append("WHERE ");
				for (Restriction column : restrictions_OR)
					query.append(column.getColumnName()).append(" ").append(column.getRestriction()).append(" ").append("?").append(" or ");

				int indexOr = query.lastIndexOf("or");
				query.delete(indexOr, indexOr + 2);

				logger.debug(query.toString());
			}
		}

		return query.toString();

	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public TreeSet<String> getResultColumns() {
		return resultColumns;
	}

	public void setResultColumns(TreeSet<String> resultColumns) {
		this.resultColumns = resultColumns;
	}

	public Set<Restriction> getRestrictions() {
		return restrictions;
	}

	public void setRestrictions(Set<Restriction> restrictions) {
		this.restrictions = restrictions;
	}

	public Set<Restriction> getRestrictions_OR() {
		return restrictions_OR;
	}

	public void setRestrictions_OR(Set<Restriction> restrictions_OR) {
		this.restrictions_OR = restrictions_OR;
	}

	public Criteria(String tableName) {
		super();
		this.tableName = tableName;
	}

}
