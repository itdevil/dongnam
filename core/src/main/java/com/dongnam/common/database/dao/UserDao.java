package com.dongnam.common.database.dao;

import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.sql.SQLTimeoutException;

import com.dongnam.common.database.GenericDao;
import com.dongnam.common.database.entitymodel.User;

public interface UserDao {

	public User getUser(String userName)throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException;
	
}
