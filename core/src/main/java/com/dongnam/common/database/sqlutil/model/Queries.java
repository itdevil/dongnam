package com.dongnam.common.database.sqlutil.model;

import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement (name="Queries")
@XmlAccessorType(XmlAccessType.FIELD)
public class Queries {

	
	private Map<String,String> sqlcommand;

	public Map<String,String> getSqlcommand() {
		return sqlcommand;
	}
	
	public void setSqlcommand(Map<String,String> sqlcommand) {
		this.sqlcommand = sqlcommand;
	}

	public Queries(Map<String,String> sqlcommand) {
		super();
		this.sqlcommand = sqlcommand;
	}

	public Queries() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Queries [sqlcommand=" + sqlcommand + "]";
	}
	
	
}
