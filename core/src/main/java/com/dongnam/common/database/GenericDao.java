package com.dongnam.common.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.sql.SQLTimeoutException;
import java.util.Date;
import java.util.List;

import com.dongnam.common.database.entitymodel.BaseEnity;

public abstract class GenericDao<T> {

	public String username;
	public Connection connection;

	public GenericDao(Connection connection) {
		// TODO Auto-generated constructor stub
		this.connection = connection;
	}

	public GenericDao(Connection connection, String username) {
		// TODO Auto-generated constructor stub
		this.connection = connection;
		this.username = username;

	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	public void closeStatment(PreparedStatement statement, ResultSet resultSet) {
		if (resultSet != null)
			try {
				resultSet.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		if (statement != null)
			try {
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
	}

	/**
	 * insert a Object to database
	 * 
	 * @param data
	 * @throws Exception
	 */
	abstract public int insert(T data) throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException;

	/**
	 * insert a list object to database
	 * 
	 * @param entities
	 * @throws Exception
	 */
	abstract public int insertBatch(List<T> entities)
			throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException;

	/**
	 * update a Object to database
	 * 
	 * @param entity
	 * @throws Exception
	 */
	abstract public int update(T entity) throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException;

	/**
	 * update a list object to database
	 * 
	 * @param entities
	 * @throws Exception
	 */
	abstract public int updateBatch(List<T> entities)
			throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException;

	/**
	 * delete a object from database
	 * 
	 * @param entity
	 * @throws Exception
	 */
	abstract public int delete(T entity) throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException;

	/**
	 * delete a list object from database
	 */
	abstract public int deleteBatch(List<T> entities)
			throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException;

	/**
	 * get all record in database
	 * 
	 * @return
	 * @throws Exception
	 */
	abstract public List<T> getList() throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException;

	/**
	 * get all record in database
	 * 
	 * @return
	 * @throws Exception
	 */
	abstract public T getRecord(Long id) throws SQLTimeoutException, SQLFeatureNotSupportedException, SQLException;

	public void setBaseInformation(BaseEnity entity) {
		
		System.out.println(entity.toString());
		if (entity.getCreate_on() == null)
			entity.setCreate_on(new Date());

		entity.setUpdate_on(new Date());

		if (entity.getCreate_by() == null)
			entity.setCreate_by(username);

		entity.setUpdate_by(username);
	}

	public void setBaseValueFromResultset(BaseEnity entity, ResultSet resultSet) throws SQLException {

		entity.setCreate_on(resultSet.getDate("create_on"));

		entity.setUpdate_on(resultSet.getDate("update_on"));

		entity.setCreate_by(resultSet.getString("create_by"));

		entity.setUpdate_by(resultSet.getString("update_by"));
	}

}
