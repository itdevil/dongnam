package com.dongnam.common.database;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.dbcp.BasicDataSource;

import com.dongnam.common.utils.CommonConfigUtil;

public class DongNamDataSource {

	private static DongNamDataSource datasource;
	private BasicDataSource ds;

	private DongNamDataSource() {

		CommonConfigUtil appconfig = CommonConfigUtil.getInstance();
		ds = new BasicDataSource();
		ds.setDriverClassName(appconfig.getProp().getProperty("driverclass"));
		ds.setUsername(appconfig.getProp().getProperty("user"));
		ds.setPassword(appconfig.getProp().getProperty("password"));
		ds.setUrl(appconfig.getProp().getProperty("url"));

		// the settings below are optional -- dbcp can work with defaults
		ds.setMinIdle(50);
		ds.setMaxIdle(100);
		ds.setMaxActive(150);

		ds.setMaxWait(60 * 1000);

		ds.setTestOnBorrow(true);
		ds.setTestWhileIdle(true);

		ds.setMinEvictableIdleTimeMillis(1000);

	}

	public static DongNamDataSource getInstance() {
		if (datasource == null) {
			datasource = new DongNamDataSource();
			return datasource;
		} else {
			return datasource;
		}
	}

	public Connection getConnection() throws SQLException {
		return this.ds.getConnection();
	}

}
