package com.dongnam.common.database.entitymodel;

public class Product extends BaseEnity{
	
	final public static String HAS_SERIAL_NUMNER ="HAS_SERIAL_NUMBER";
	final public static String NO_SERIAL_NUMNER ="NO_SERIAL_NUMNER";
	final public static String LICENSE_KEY ="LICENSE_KEY";
	final public static String SERVICE ="SERVICE";
	
	
	private long id;
	private String shortname;
	private String fullname;
	private String unit;
	private String comment;
	private String loai;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getShortname() {
		return shortname;
	}
	public void setShortname(String shortname) {
		this.shortname = shortname;
	}
	public String getFullname() {
		return fullname;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getLoai() {
		return loai;
	}
	public void setLoai(String loai) {
		this.loai = loai;
	}
	@Override
	public String toString() {
		return "Product [id=" + id + ", shortname=" + shortname + ", fullname=" + fullname + ", unit=" + unit
				+ ", comment=" + comment + ", loai=" + loai + "]";
	}
	public Product() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((shortname == null) ? 0 : shortname.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (shortname == null) {
			if (other.shortname != null)
				return false;
		} else if (!shortname.equals(other.shortname))
			return false;
		return true;
	}
	public Product(long id, String shortname, String fullname, String unit, String comment, String loai) {
		super();
		this.id = id;
		this.shortname = shortname;
		this.fullname = fullname;
		this.unit = unit;
		this.comment = comment;
		this.loai = loai;
	}
	
	
	
	
}
