package com.dongnam.common.database;

public class Restriction implements Comparable<Restriction> {

	public static final String eq = "=";
	public static final String like = "like";

	private String columnName;
	private String restriction;
	private Object value;

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public String getRestriction() {
		return restriction;
	}

	public void setRestriction(String restriction) {
		this.restriction = restriction;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public Restriction(String columnName, String restriction, Object value) {
		super();
		this.columnName = columnName;
		this.restriction = restriction;
		this.value = value;
	}

	@Override
	public String toString() {
		return "Restriction [columnName=" + columnName + ", restriction=" + restriction + ", value=" + value + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((columnName == null) ? 0 : columnName.hashCode());
		result = prime * result + ((restriction == null) ? 0 : restriction.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Restriction other = (Restriction) obj;
		if (columnName == null) {
			if (other.columnName != null)
				return false;
		} else if (!columnName.equals(other.columnName))
			return false;

		return true;
	}

	public int compareTo(Restriction o) {
		// TODO Auto-generated method stub

		return this.getColumnName().compareToIgnoreCase(o.getColumnName());
	}

}
