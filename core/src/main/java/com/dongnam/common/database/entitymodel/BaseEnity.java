package com.dongnam.common.database.entitymodel;

import java.util.Date;

public class BaseEnity {
	
	private String create_by;
	private Date create_on;
	private String update_by;
	private Date update_on;
	
	public String getCreate_by() {
		return create_by;
	}
	public void setCreate_by(String create_by) {
		this.create_by = create_by;
	}
	public Date getCreate_on() {
		return create_on;
	}
	public void setCreate_on(Date create_on) {
		this.create_on = create_on;
	}
	public String getUpdate_by() {
		return update_by;
	}
	public void setUpdate_by(String update_by) {
		this.update_by = update_by;
	}
	public Date getUpdate_on() {
		return update_on;
	}
	public void setUpdate_on(Date update_on) {
		this.update_on = update_on;
	}
	
	

}
