Document
Non-functional Requirements :

Java 1.8
Applucation Server : spring boot 1.4.0.RELEASE
Port : 8090
Jar File named : stockPriceService.jar

For Deployer
+ Build Source code by maven
+ To run the application by command lne below :
   java -jar stockPriceService.jar
+ url for view api description : 
   http://localhost:8090/stockPriceService-service/swagger-ui.html

For developer
+ Could configure something on application.properties in resources folder

server.port=${port:8090}
app.name=Micro Service
app.description=${app.name} is a Spring Boot application
server.contextPath=/stockPriceService-service

logging.level.com.sentifi.core=INFO
logging.level.com.sentifi.core.controllers=DEBUG
logging.level.com.sentifi.core.thread=DEBUG
logging.level.com.sentifi.core.cache=DEBUG

# custom configure
numberMostCacheTicker=3 : number ticker could be cached
numberTickersForGetClosePrices = 10 : number ticker fro api get200dma for all

